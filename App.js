
import React, { Component } from 'react'

import {

  YellowBox
} from 'react-native';

import { createRootNavigator } from "./src/Routes";



export default class App extends Component {

  render() {
    const Layout = createRootNavigator()

    return (
      <Layout />

    );
  }
}

