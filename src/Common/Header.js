import React, { Component } from 'react'
import {
    View,
    Image,
    Text,
} from 'react-native'
import { colors, dimensions } from '../styles/base'
const user_icon = require('../assets/ic_user.png')

class HeaderComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }


    render() {

        const { title } = this.props
        return (
            <View style={{
            }}>
                {
                    title == "SHOP" ?
                        <View style={Styles.HeaderStyle}>

                            <View style={{ flexDirection: 'row', flex: 0.6, }}>

                                <View style={Styles.secondView}>
                                    <Text style={{ color: colors.white, fontSize: 12, fontWeight: 'bold' }}>
                                        589,497 LBS CAPTURED
                                    </Text>
                                </View>

                                <View style={{ alignSelf: 'center', top: 10, left: 25, }}>
                                    <Text style={{ color: colors.white, fontSize: 16, fontWeight: 'bold' }}>
                                        SHOP
                                    </Text>
                                </View>

                            </View>

                            <View style={{ flex: 0.4, alignSelf: 'center' }}>
                                <Image source={user_icon} style={{ alignSelf: 'flex-end', right: 20, top: 10 }} />
                            </View>
                        </View>
                        :
                        null

                }


            </View>
        );
    }
}

const Styles = {
    HeaderStyle: { backgroundColor: colors.darkblue, height: 80, flexDirection: 'row' },
    secondView: { left: 10, backgroundColor: colors.lightblue, borderColor: '#ffffff', borderRadius: 2, borderWidth: 1, padding: 5, alignSelf: 'center', top: 10 },
    HeaderText: {
        fontSize: 18,
        color: colors.white,
        alignSelf: 'center',
        fontWeight: '700'
    }
}

export default HeaderComponent
