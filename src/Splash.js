import React, { Component } from 'react'

import {
  ImageBackground, Dimensions, Platform, StatusBar, View, Text
} from 'react-native'

import { colors } from "./styles/base";
const { height, width } = Dimensions.get('window');


class Splash extends Component {
  constructor(props) {
    super(props)
  }


  componentDidMount() {

    setTimeout(() => {

      this.props.navigation.navigate('Coupen');
    }, 2000)

  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <Text style={styles.textStyle}>
          Splash Screen
     </Text>

      </View>
    );
  }
}


export default Splash

const styles = {
  mainContainer: { flex: 1, backgroundColor: colors.darkblue, justifyContent: 'center' },
  textStyle: { alignSelf: 'center', color: '#ffffff' }
}